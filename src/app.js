'use strict';
const moment = require('moment');
const diff = require('diff');

const { App, Project } = require('jovo-framework');
const { Alexa } = require('jovo-platform-alexa');
const { GoogleAssistant } = require('jovo-platform-googleassistant');
const { JovoDebugger } = require('jovo-plugin-debugger');
const { FileDb } = require('jovo-db-filedb');
const { MongoDb } = require('jovo-db-mongodb');
const url = 'mongodb://localhost:27017';
const mongoClient = require('mongodb').MongoClient;

// ------------------------------------------------------------------
// APP INITIALIZATION
// ------------------------------------------------------------------

const app = new App();

app.use(
  new Alexa(),
  new GoogleAssistant(),
  new JovoDebugger(),
  new FileDb(),
  new MongoDb()
);

var a = "";
var b = "";
var zm2 = "";
var r = "";

// mongoClient.connect("mongodb://localhost:27017/movies", {
//   useNewUrlParser: true,
//   useUnifiedTopology: true
// }, function (error, db) {
//   if (error)
//     console.log('nie dziala');
//   var dbo = db.db("movies");
//   var query = { title: { $ne: null } };
//   var n = dbo.collection("table").countDocuments(query);
//   r = Math.floor(Math.random() * n);
// });

var n = 5;
r = Math.floor(Math.random() * n);

// ------------------------------------------------------------------
// APP LOGIC
// ------------------------------------------------------------------

app.setHandler({
  LAUNCH() {
    this.$user.$data.previousSeenAt = this.$user.$data.lastSeenAt;
    this.$user.$data.lastSeenAt = moment().toISOString();
    if (!this.$user.$data.previousSeenAt) {
      this.$user.$data.previousSeenAt = this.$user.$data.lastSeenAt;
    }
    this.toIntent('HelloIntent');
  },

  HelloIntent() {
    // var a = moment(this.$user.$data.lastSeenAt);
    // var b = moment(this.$user.$data.previousSeenAt);
    // this.$user.$data.temp = a.diff(b, 'seconds');
    // if (this.$user.$data.name) {
    //   if (this.$user.$data.temp < 9000) {
    //     this.$speech.addText([
    //       'You come back quickly ' + this.$user.$data.name + '.',
    //       'You\'ve already missed me ' + this.$user.$data.name + '?'
    //     ]);
    //     this.toIntent('PreferencesIntent').tell(this.$speech);
    //   } else if (this.$user.$data.temp >= 9000 && this.$user.$data.temp <= 604800) {
    //     this.$speech.addText([
    //       //'Hello, welcome back to the Movie App!',
    //       'Hello ' + this.$user.$data.name + ', welcome back to the Movie App!',
    //       'Hey ' + this.$user.$data.name + ', I\'m glad to hear your voice again!',
    //       'Hi ' + this.$user.$data.name + ', I\'m glad to hear you again!',
    //       'Hi ' + this.$user.$data.name + '!',
    //       'Hello ' + this.$user.$data.name + '!',
    //       'Hey ' + this.$user.$data.name + '!',
    //     ]);
    //     this.toIntent('PreferencesIntent').tell(this.$speech);
    //   } else {
    //     this.$speech.addText([
    //       'Hello ' + this.$user.$data.name + '! It\'s been a while.',
    //       'Hello ' + this.$user.$data.name + ', you came back.',
    //     ]);
    //     this.toIntent('PreferencesIntent').tell(this.$speech);
    //   }
    //   this.$user.getId();
    // } else {
    if (this.$user.$data.name) {
      this.$speech.addText([
        'Hello ' + this.$user.$data.name + ', welcome back to the Movie App!',
        'Hey ' + this.$user.$data.name + ', I\'m glad to hear your voice again!',
        'Hi ' + this.$user.$data.name + ', I\'m glad to hear you again!'
      ]);
      this.toIntent('PreferencesIntent').tell(this.$speech);
    } else {
      this.ask('Welcome to the The Movie App! I\'ll help you choose the perfect movie. Let\'s get to know each other better. What\'s your name?');
    }
  },

  NEW_USER() {
    return this.ask('Welcome to the The Movie App! I\'ll help you choose the perfect movie. Let\'s get to know each other better. What\'s your name?');
  },

  MyNameIsIntent() {
    if (this.$inputs.name.value === 'no' || this.$inputs.name.value === 'nope' || this.$inputs.name.value === 'i don\'t want to tell' || this.$inputs.name.value === 'i won\'t tell my name') {
      this.$speech.addText([
        'If you don\'t tell me your name, I can\'t recommend you movies especially for you. So, what\'s your name?',
        'Please tell me your name',
      ]);
      this.ask(this.$speech);
    } else {
      this.$user.$data.name = this.$inputs.name.value;
      this.ask('Hey ' + this.$inputs.name.value + ', nice to meet you! I\’ll try to find the perfect movie for you based on your answers. To make it easier in the future, tell me what kind of movies do you like to watch?');
    }
  },

  GenreIntent() {
    a = moment().toISOString();
    if (this.$user.$data.genre == null) {
      this.$user.$data.genre = [this.$inputs.genre.value];
    } else {
      if ((this.$user.$data.genre).includes(this.$inputs.genre.value)) {
      } else {
        var obj = this.$user.$data.genre;
        obj.push(this.$inputs.genre.value);
      }
    }
    this.followUpState('RecomendationState').toIntent('GenreRecomendationIntent');
  },

  TwoGenresIntent() {
    a = moment().toISOString();
    if ((this.$user.$data.genre == null) && (this.$user.$data.genretwo == null)) {
      this.$user.$data.genre = [this.$inputs.genre.value];
      this.$user.$data.genretwo = [this.$inputs.genretwo.value];
    } else {
      if ((this.$user.$data.genre).includes(this.$inputs.genre.value) && (this.$user.$data.genretwo).includes(this.$inputs.genretwo.value)) {
      } else {
        var obj = this.$user.$data.genre;
        obj.push(this.$inputs.genre.value);
        var objtwo = this.$user.$data.genretwo;
        objtwo.push(this.$inputs.genretwo.value);
      }
    }
    this.followUpState('RecomendationState').toIntent('TwoGenresRecomendationIntent');
  },

  YearIntent() {
    a = moment().toISOString();
    if (this.$user.$data.year == null) {
      this.$user.$data.year = [this.$inputs.year.value];
    } else {
      if ((this.$user.$data.year).includes(this.$inputs.year.value)) {
      } else {
        var obj = this.$user.$data.year;
        obj.push(this.$inputs.year.value);
      }
    }
    this.followUpState('RecomendationState').toIntent('YearRecomendationIntent');
  },

  TwoYearsIntent() {
    a = moment().toISOString();
    if ((this.$user.$data.year == null) && (this.$user.$data.yeartwo == null)) {
      this.$user.$data.year = [this.$inputs.year.value];
      this.$user.$data.yeartwo = [this.$inputs.yeartwo.value];
    } else {
      if ((this.$user.$data.year).includes(this.$inputs.year.value) && (this.$user.$data.yeartwo).includes(this.$inputs.yeartwo.value)) {
      } else {
        var obj = this.$user.$data.year;
        obj.push(this.$inputs.year.value);
        var objtwo = this.$user.$data.yeartwo;
        objtwo.push(this.$inputs.yeartwo.value);
      }
    }
    this.followUpState('RecomendationState').toIntent('TwoYearsRecomendationIntent');
  },

  ActorIntent() {
    a = moment().toISOString();
    if (this.$user.$data.actor == null) {
      this.$user.$data.actor = [this.$inputs.actor.value];
    } else {
      if ((this.$user.$data.actor).includes(this.$inputs.actor.value)) {
      } else {
        var obj = this.$user.$data.actor;
        obj.push(this.$inputs.actor.value);
      }
    }
    this.followUpState('RecomendationState').toIntent('ActorRecomendationIntent');
  },

  TwoActorsIntent() {
    a = moment().toISOString();
    if ((this.$user.$data.actor == null) && (this.$user.$data.actortwo == null)) {
      this.$user.$data.actor = [this.$inputs.actor.value];
      this.$user.$data.actortwo = [this.$inputs.actortwo.value];
    } else {
      if ((this.$user.$data.actor).includes(this.$inputs.actor.value) && (this.$user.$data.actortwo).includes(this.$inputs.actortwo.value)) {
      } else {
        var obj = this.$user.$data.actor;
        obj.push(this.$inputs.actor.value);
        var objtwo = this.$user.$data.actortwo;
        objtwo.push(this.$inputs.actortwo.value);
      }
    }
    this.followUpState('RecomendationState').toIntent('TwoActorsRecomendationIntent');
  },

  ActressIntent() {
    a = moment().toISOString();
    if (this.$user.$data.actress == null) {
      this.$user.$data.actress = [this.$inputs.actress.value];
    } else {
      if ((this.$user.$data.actress).includes(this.$inputs.actress.value)) {
      } else {
        var obj = this.$user.$data.actress;
        obj.push(this.$inputs.actress.value);
      }
    }
    this.followUpState('RecomendationState').toIntent('ActressRecomendationIntent');
  },

  TwoActressesIntent() {
    a = moment().toISOString();
    if ((this.$user.$data.actress == null) && (this.$user.$data.actresstwo == null)) {
      this.$user.$data.actress = [this.$inputs.actress.value];
      this.$user.$data.actresstwo = [this.$inputs.actresstwo.value];
    } else {
      if ((this.$user.$data.actress).includes(this.$inputs.actress.value) && (this.$user.$data.actresstwo).includes(this.$inputs.actresstwo.value)) {
      } else {
        var obj = this.$user.$data.actress;
        obj.push(this.$inputs.actress.value);
        var objtwo = this.$user.$data.actresstwo;
        objtwo.push(this.$inputs.actresstwo.value);
      }
    }
    this.followUpState('RecomendationState').toIntent('TwoActressesRecomendationIntent');
  },

  KeywordsIntent() {
    a = moment().toISOString();
    if (this.$user.$data.keyword == null) {
      this.$user.$data.keyword = [this.$inputs.keyword.value];
    } else {
      if ((this.$user.$data.keyword).includes(this.$inputs.keyword.value)) {
      } else {
        var obj = this.$user.$data.keyword;
        obj.push(this.$inputs.keyword.value);
      }
    }
    this.followUpState('RecomendationState').toIntent('KeywordsRecomendationIntent');
  },

  TwoKeywordsIntent() {
    a = moment().toISOString();
    if ((this.$user.$data.keyword == null) && (this.$user.$data.keywordtwo == null)) {
      this.$user.$data.keyword = [this.$inputs.keyword.value];
      this.$user.$data.keywordtwo = [this.$inputs.keywordtwo.value];
    } else {
      if ((this.$user.$data.keyword).includes(this.$inputs.keyword.value) && (this.$user.$data.keywordtwo).includes(this.$inputs.keywordtwo.value)) {
      } else {
        var obj = this.$user.$data.keyword;
        obj.push(this.$inputs.keyword.value);
        var objtwo = this.$user.$data.keywordtwo;
        objtwo.push(this.$inputs.keywordtwo.value);
      }
    }
    this.followUpState('RecomendationState').toIntent('TwoKeywordsRecomendationIntent');
  },

  LanguageIntent() {
    a = moment().toISOString();
    if (this.$user.$data.language == null) {
      this.$user.$data.language = [this.$inputs.language.value];
    } else {
      if ((this.$user.$data.language).includes(this.$inputs.language.value)) {
      } else {
        var obj = this.$user.$data.language;
        obj.push(this.$inputs.language.value);
      }
    }
    this.followUpState('RecomendationState').toIntent('LanguageRecomendationIntent');
  },

  TwoLanguagesIntent() {
    a = moment().toISOString();
    if ((this.$user.$data.language == null) && (this.$user.$data.languagetwo == null)) {
      this.$user.$data.language = [this.$inputs.language.value];
      this.$user.$data.languagetwo = [this.$inputs.languagetwo.value];
    } else {
      if ((this.$user.$data.language).includes(this.$inputs.language.value) && (this.$user.$data.languagetwo).includes(this.$inputs.languagetwo.value)) {
      } else {
        var obj = this.$user.$data.language;
        obj.push(this.$inputs.language.value);
        var objtwo = this.$user.$data.languagetwo;
        objtwo.push(this.$inputs.languagetwo.value);
      }
    }
    this.followUpState('RecomendationState').toIntent('TwoLanguagesRecomendationIntent');
  },

  ProductionIntent() {
    a = moment().toISOString();
    if (this.$user.$data.production_company == null) {
      this.$user.$data.production_company = [this.$inputs.production_company.value];
    } else {
      if ((this.$user.$data.production_company).includes(this.$inputs.production_company.value)) {
      } else {
        var obj = this.$user.$data.production_company;
        obj.push(this.$inputs.production_company.value);
      }
    }
    this.followUpState('RecomendationState').toIntent('ProductionRecomendationIntent');
  },

  TwoProductionsIntent() {
    a = moment().toISOString();
    if ((this.$user.$data.production_company == null) && (this.$user.$data.production_companytwo == null)) {
      this.$user.$data.production_company = [this.$inputs.production_company.value];
      this.$user.$data.production_companytwo = [this.$inputs.production_companytwo.value];
    } else {
      if ((this.$user.$data.production_company).includes(this.$inputs.production_company.value) && (this.$user.$data.production_companytwo).includes(this.$inputs.production_companytwo.value)) {
      } else {
        var obj = this.$user.$data.production_company;
        obj.push(this.$inputs.production_company.value);
        var objtwo = this.$user.$data.production_companytwo;
        objtwo.push(this.$inputs.production_companytwo.value);
      }
    }
    this.followUpState('RecomendationState').toIntent('TwoProductionsRecomendationIntent');
  },

  CountryIntent() {
    a = moment().toISOString();
    if (this.$user.$data.country == null) {
      this.$user.$data.country = [this.$inputs.country.value];
    } else {
      if ((this.$user.$data.country).includes(this.$inputs.country.value)) {
      } else {
        var obj = this.$user.$data.country;
        obj.push(this.$inputs.country.value);
      }
    }
    this.followUpState('RecomendationState').toIntent('CountryRecomendationIntent');
  },

  TwoCountriesIntent() {
    a = moment().toISOString();
    if ((this.$user.$data.country == null) && (this.$user.$data.countrytwo == null)) {
      this.$user.$data.country = [this.$inputs.country.value];
      this.$user.$data.countrytwo = [this.$inputs.countrytwo.value];
    } else {
      if ((this.$user.$data.country).includes(this.$inputs.country.value) && (this.$user.$data.countrytwo).includes(this.$inputs.countrytwo.value)) {
      } else {
        var obj = this.$user.$data.country;
        obj.push(this.$inputs.country.value);
        var objtwo = this.$user.$data.countrytwo;
        objtwo.push(this.$inputs.countrytwo.value);
      }
    }
    this.followUpState('RecomendationState').toIntent('TwoCountriesRecomendationIntent');
  },

  BetweenIntent() {
    a = moment().toISOString();
    if ((this.$user.$data.year == null) && (this.$user.$data.yeartwo == null)) {
      this.$user.$data.year = [this.$inputs.year.value];
      this.$user.$data.yeartwo = [this.$inputs.yeartwo.value];
    } else {
      if ((this.$user.$data.year).includes(this.$inputs.year.value) && (this.$user.$data.yeartwo).includes(this.$inputs.yeartwo.value)) {
      } else {
        var obj = this.$user.$data.year;
        obj.push(this.$inputs.year.value);
        var objtwo = this.$user.$data.yeartwo;
        objtwo.push(this.$inputs.yeartwo.value);
      }
    }
    this.followUpState('RecomendationState').toIntent('BetweenRecomendationIntent');
  },

  GenreAndActorIntent() {
    a = moment().toISOString();
    if ((this.$user.$data.genre == null) && (this.$user.$data.actor == null)) {
      this.$user.$data.genre = [this.$inputs.genre.value];
      this.$user.$data.actor = [this.$inputs.actor.value];
    } else {
      if ((this.$user.$data.genre).includes(this.$inputs.genre.value) && (this.$user.$data.actor).includes(this.$inputs.actor.value)) {
      } else {
        var obj = this.$user.$data.genre;
        obj.push(this.$inputs.genre.value);
        var objtwo = this.$user.$data.actor;
        objtwo.push(this.$inputs.actor.value);
      }
    }
    this.followUpState('RecomendationState').toIntent('GenreAndActorRecomendationIntent');
  },

  ActorAndLanguageIntent() {
    a = moment().toISOString();
    if ((this.$user.$data.actor == null) && (this.$user.$data.language == null)) {
      this.$user.$data.actor = [this.$inputs.actor.value];
      this.$user.$data.language = [this.$inputs.language.value];
    } else {
      if ((this.$user.$data.actor).includes(this.$inputs.actor.value) && (this.$user.$data.language).includes(this.$inputs.language.value)) {
      } else {
        var obj = this.$user.$data.actor;
        obj.push(this.$inputs.actor.value);
        var objtwo = this.$user.$data.language;
        objtwo.push(this.$inputs.language.value);
      }
    }
    this.followUpState('RecomendationState').toIntent('ActorAndLanguageRecomendationIntent');
  },

  KeywordsOrGenreIntent() {
    a = moment().toISOString();
    if ((this.$user.$data.keyword == null) && (this.$user.$data.genre == null)) {
      this.$user.$data.keyword = [this.$inputs.keyword.value];
      this.$user.$data.genre = [this.$inputs.genre.value];
    } else {
      if ((this.$user.$data.keyword).includes(this.$inputs.keyword.value) && (this.$user.$data.genre).includes(this.$inputs.genre.value)) {
      } else {
        var obj = this.$user.$data.keyword;
        obj.push(this.$inputs.keyword.value);
        var objtwo = this.$user.$data.genre;
        objtwo.push(this.$inputs.genre.value);
      }
    }
    this.followUpState('RecomendationState').toIntent('KeywordsOrGenreRecomendationIntent');
  },

  LanguageOrCountryIntent() {
    a = moment().toISOString();
    if ((this.$user.$data.language == null) && (this.$user.$data.country == null)) {
      this.$user.$data.language = [this.$inputs.language.value];
      this.$user.$data.country = [this.$inputs.country.value];
    } else {
      if ((this.$user.$data.language).includes(this.$inputs.language.value) && (this.$user.$data.country).includes(this.$inputs.country.value)) {
      } else {
        var obj = this.$user.$data.language;
        obj.push(this.$inputs.language.value);
        var objtwo = this.$user.$data.country;
        objtwo.push(this.$inputs.country.value);
      }
    }
    this.followUpState('RecomendationState').toIntent('LanguageOrCountryRecomendationIntent');
  },

  ProductionAndYearIntent() {
    a = moment().toISOString();
    if ((this.$user.$data.production_company == null) && (this.$user.$data.year == null) && (this.$user.$data.yeartwo == null)) {
      this.$user.$data.production_company = [this.$inputs.production_company.value];
      this.$user.$data.year = [this.$inputs.year.value];
      this.$user.$data.yeartwo = [this.$inputs.yeartwo.value];
    } else {
      if ((this.$user.$data.production_company).includes(this.$inputs.production_company.value) && (this.$user.$data.year).includes(this.$inputs.year.value) && (this.$user.$data.yeartwo).includes(this.$inputs.yeartwo.value)) {
      } else {
        var obj = this.$user.$data.production_company;
        obj.push(this.$inputs.production_company.value);
        var objone = this.$user.$data.year;
        objone.push(this.$inputs.year.value);
        var objtwo = this.$user.$data.yeartwo;
        objtwo.push(this.$inputs.yeartwo.value);
      }
    }
    this.followUpState('RecomendationState').toIntent('ProductionAndYearRecomendationIntent');
  },

  CountryOrYearIntent() {
    a = moment().toISOString();
    if ((this.$user.$data.country == null) && (this.$user.$data.year == null)) {
      this.$user.$data.country = [this.$inputs.country.value];
      this.$user.$data.year = [this.$inputs.year.value];
    } else {
      if ((this.$user.$data.country).includes(this.$inputs.country.value) && (this.$user.$data.year).includes(this.$inputs.year.value)) {
      } else {
        var obj = this.$user.$data.country;
        obj.push(this.$inputs.country.value);
        var objtwo = this.$user.$data.year;
        objtwo.push(this.$inputs.year.value);
      }
    }
    this.followUpState('RecomendationState').toIntent('CountryOrYearRecomendationIntent');
  },

  PreferencesIntent() {
    this.$speech.addText([
      'What kind of movie do you want to find now?',
      'What would you like to see this time?',
      'What movie can I recommend to you today?',
      'What would you like to see today?',
      'What can I recommend to you today?',
      'What\'d you like to see today?',
      'What kind of films should I look for?',
      'What movie would you like to see today?',
      'What can I do for you today?',
      'What can I find for you today?'
    ]);
    this.ask(this.$speech);
  },

  //Intent do testowania bazy
  TestIntent() {
    this.followUpState('RecomendationState').toIntent('RecomendationIntent');
  },

  RecomendationState: {
    async GenreRecomendationIntent() {
      function getData() {
        return new Promise((resolve, reject) => {
          mongoClient.connect("mongodb://localhost:27017/movies", {
            useNewUrlParser: true,
            useUnifiedTopology: true
          }, function (error, db) {
            if (error)
              console.log('nie dziala');
            var dbo = db.db("movies");
            // --$match--
            var moviegenre = { genres: /Comedy/ };
            dbo.collection("table").find(moviegenre).project({ _id: 0, title: 1 }).limit(1).skip(r).next(function (err, result) {
              if (err)
                throw err;
              console.log(JSON.stringify(result.title));
              console.log(result.title);
              zm2 = result.title;
              resolve();
            });
          });
        }
        )
      };
      await getData();
      this.followUpState('RecomendationState').toIntent('MovieRecomendationIntent');
    },

    async TwoGenresRecomendationIntent() {
      function getData() {
        return new Promise((resolve, reject) => {
          mongoClient.connect("mongodb://localhost:27017/movies", {
            useNewUrlParser: true,
            useUnifiedTopology: true
          }, function (error, db) {
            if (error)
              console.log('nie dziala');
            var dbo = db.db("movies");
            // --$or--
            // var genreone = { genres: /Comedy/ };
            // var genretwo = { genres: /Drama/ };
            // dbo.collection("table").find({ $and: [{ $or: [genreone, genretwo] }] }).project({ _id: 0, title: 1 }).limit(1).skip(r).next(function (err, result) {
            //   if (err)
            //     throw err;
            //   console.log(JSON.stringify(result.title));
            //   console.log(result.title);
            //   zm2 = result.title;
            //   resolve();
            // });
            // --$and--
            var genreone = { genres: /Comedy/ };
            var genretwo = { genres: /Adventure/ };
            dbo.collection("table").find({ $and: [genreone, genretwo] }).project({ _id: 0, title: 1 }).limit(1).skip(r).next(function (err, result) {
              if (err)
                throw err;
              console.log(JSON.stringify(result.title));
              console.log(result.title);
              zm2 = result.title;
              resolve();
            });
          });
        }
        )
      };
      await getData();
      this.followUpState('RecomendationState').toIntent('MovieRecomendationIntent');
    },

    async YearRecomendationIntent() {
      function getData() {
        return new Promise((resolve, reject) => {
          mongoClient.connect("mongodb://localhost:27017/movies", {
            useNewUrlParser: true,
            useUnifiedTopology: true
          }, function (error, db) {
            if (error)
              console.log('nie dziala');
            var dbo = db.db("movies");
            // --$match--
            var releasedate = { release_date: /2014/ };
            dbo.collection("table").find(releasedate).project({ _id: 0, title: 1 }).limit(1).skip(r).next(function (err, result) {
              if (err)
                throw err;
              console.log(JSON.stringify(result.title));
              console.log(result.title);
              zm2 = result.title;
              resolve();
            });
          });
        }
        )
      };
      await getData();
      this.followUpState('RecomendationState').toIntent('MovieRecomendationIntent');
    },

    async TwoYearsRecomendationIntent() {
      function getData() {
        return new Promise((resolve, reject) => {
          mongoClient.connect("mongodb://localhost:27017/movies", {
            useNewUrlParser: true,
            useUnifiedTopology: true
          }, function (error, db) {
            if (error)
              console.log('nie dziala');
            var dbo = db.db("movies");
            // --$or--
            var yearone = { release_date: /2014/ };
            var yeartwo = { release_date: /2015/ };
            dbo.collection("table").find({ $and: [{ $or: [yearone, yeartwo] }] }).project({ _id: 0, title: 1 }).limit(1).skip(r).next(function (err, result) {
              if (err)
                throw err;
              console.log(JSON.stringify(result.title));
              console.log(result.title);
              zm2 = result.title;
              resolve();
            });
          });
        }
        )
      };
      await getData();
      this.followUpState('RecomendationState').toIntent('MovieRecomendationIntent');
    },

    async ActorRecomendationIntent() {
      function getData() {
        return new Promise((resolve, reject) => {
          mongoClient.connect("mongodb://localhost:27017/movies", {
            useNewUrlParser: true,
            useUnifiedTopology: true
          }, function (error, db) {
            if (error)
              console.log('nie dziala');
            var dbo = db.db("movies");
            // --$match--
            var movieactor = { cast: /Brad Pitt/ };
            dbo.collection("table").find(movieactor).project({ _id: 0, title: 1 }).limit(1).skip(r).next(function (err, result) {
              if (err)
                throw err;
              console.log(JSON.stringify(result.title));
              console.log(result.title);
              zm2 = result.title;
              resolve();
            });
          });
        }
        )
      };
      await getData();
      this.followUpState('RecomendationState').toIntent('MovieRecomendationIntent');
    },

    async TwoActorsRecomendationIntent() {
      function getData() {
        return new Promise((resolve, reject) => {
          mongoClient.connect("mongodb://localhost:27017/movies", {
            useNewUrlParser: true,
            useUnifiedTopology: true
          }, function (error, db) {
            if (error)
              console.log('nie dziala');
            var dbo = db.db("movies");
            // --$or--
            // var actorone = { cast: /Brad Pitt/ };
            // var actortwo = { cast: /Nicolas Cage/ };
            // dbo.collection("table").find({ $and: [{ $or: [actorone, actortwo] }] }).project({ _id: 0, title: 1 }).limit(1).skip(r).next(function (err, result) {
            //   if (err)
            //     throw err;
            //   console.log(JSON.stringify(result.title));
            //   console.log(result.title);
            //   zm2 = result.title;
            //   resolve();
            // });
            // --$and--
            var actorone = { cast: /George Clooney/ };
            var actortwo = { cast: /Brad Pitt/ };
            dbo.collection("table").find({ $and: [actorone, actortwo] }).project({ _id: 0, title: 1 }).limit(1).skip(r).next(function (err, result) {
              if (err)
                throw err;
              console.log(JSON.stringify(result.title));
              console.log(result.title);
              zm2 = result.title;
              resolve();
            });
          });
        }
        )
      };
      await getData();
      this.followUpState('RecomendationState').toIntent('MovieRecomendationIntent');
    },

    async ActressRecomendationIntent() {
      function getData() {
        return new Promise((resolve, reject) => {
          mongoClient.connect("mongodb://localhost:27017/movies", {
            useNewUrlParser: true,
            useUnifiedTopology: true
          }, function (error, db) {
            if (error)
              console.log('nie dziala');
            var dbo = db.db("movies");
            // --$match--
            var movieactress = { cast: /Angelina Jolie/ };
            dbo.collection("table").find(movieactress).project({ _id: 0, title: 1 }).limit(1).skip(r).next(function (err, result) {
              if (err)
                throw err;
              console.log(JSON.stringify(result.title));
              console.log(result.title);
              zm2 = result.title;
              resolve();
            });
          });
        }
        )
      };
      await getData();
      this.followUpState('RecomendationState').toIntent('MovieRecomendationIntent');
    },

    async TwoActressesRecomendationIntent() {
      function getData() {
        return new Promise((resolve, reject) => {
          mongoClient.connect("mongodb://localhost:27017/movies", {
            useNewUrlParser: true,
            useUnifiedTopology: true
          }, function (error, db) {
            if (error)
              console.log('nie dziala');
            var dbo = db.db("movies");
            // --$or--
            var actressone = { cast: /Angelina Jolie/ };
            var actresstwo = { cast: /Julia Roberts/ };
            dbo.collection("table").find({ $and: [{ $or: [actressone, actresstwo] }] }).project({ _id: 0, title: 1 }).limit(1).skip(r).next(function (err, result) {
              if (err)
                throw err;
              console.log(JSON.stringify(result.title));
              console.log(result.title);
              zm2 = result.title;
              resolve();
            });
            // --$and--
            // var actressone = { cast: /Angelina Jolie/ };
            // var actresstwo = { cast: /Julia Roberts/ };
            // dbo.collection("table").find({ $and: [actressone, actresstwo] }).project({ _id: 0, title: 1 }).limit(1).skip(r).next(function (err, result) {
            //   if (err)
            //     throw err;
            //   console.log(JSON.stringify(result.title));
            //   console.log(result.title);
            //   zm2 = result.title;
            //   resolve();
            // });
          });
        }
        )
      };
      await getData();
      this.followUpState('RecomendationState').toIntent('MovieRecomendationIntent');
    },

    async KeywordsRecomendationIntent() {
      function getData() {
        return new Promise((resolve, reject) => {
          mongoClient.connect("mongodb://localhost:27017/movies", {
            useNewUrlParser: true,
            useUnifiedTopology: true
          }, function (error, db) {
            if (error)
              console.log('nie dziala');
            var dbo = db.db("movies");
            // --$match--
            var moviekeyword = { keywords: /detective/ };
            dbo.collection("table").find(moviekeyword).project({ _id: 0, title: 1 }).limit(1).skip(r).next(function (err, result) {
              if (err)
                throw err;
              console.log(JSON.stringify(result.title));
              console.log(result.title);
              zm2 = result.title;
              resolve();
            });
          });
        }
        )
      };
      await getData();
      this.followUpState('RecomendationState').toIntent('MovieRecomendationIntent');
    },

    async TwoKeywordsRecomendationIntent() {
      function getData() {
        return new Promise((resolve, reject) => {
          mongoClient.connect("mongodb://localhost:27017/movies", {
            useNewUrlParser: true,
            useUnifiedTopology: true
          }, function (error, db) {
            if (error)
              console.log('nie dziala');
            var dbo = db.db("movies");
            // --$or--
            // var keywordone = { keywords: /travel/ };
            // var keywordtwo = { keywords: /winter/ };
            // dbo.collection("table").find({ $and: [{ $or: [keywordone, keywordtwo] }] }).project({ _id: 0, title: 1 }).limit(1).skip(r).next(function (err, result) {
            //   if (err)
            //     throw err;
            //   console.log(JSON.stringify(result.title));
            //   console.log(result.title);
            //   zm2 = result.title;
            //   resolve();
            // });
            // --$and--
            var keywordone = { keywords: /steampunk/ };
            var keywordtwo = { keywords: /detective/ };
            dbo.collection("table").find({ $and: [keywordone, keywordtwo] }).project({ _id: 0, title: 1 }).limit(1).skip(r).next(function (err, result) {
              if (err)
                throw err;
              console.log(JSON.stringify(result.title));
              console.log(result.title);
              zm2 = result.title;
              resolve();
            });
          });
        }
        )
      };
      await getData();
      this.followUpState('RecomendationState').toIntent('MovieRecomendationIntent');
    },

    async LanguageRecomendationIntent() {
      function getData() {
        return new Promise((resolve, reject) => {
          mongoClient.connect("mongodb://localhost:27017/movies", {
            useNewUrlParser: true,
            useUnifiedTopology: true
          }, function (error, db) {
            if (error)
              console.log('nie dziala');
            var dbo = db.db("movies");
            // --$match--
            var movielanguage = { original_language: /en/ };
            dbo.collection("table").find(movielanguage).project({ _id: 0, title: 1 }).limit(1).skip(r).next(function (err, result) {
              if (err)
                throw err;
              console.log(JSON.stringify(result.title));
              console.log(result.title);
              zm2 = result.title;
              resolve();
            });
          });
        }
        )
      };
      await getData();
      this.followUpState('RecomendationState').toIntent('MovieRecomendationIntent');
    },

    async TwoLanguagesRecomendationIntent() {
      function getData() {
        return new Promise((resolve, reject) => {
          mongoClient.connect("mongodb://localhost:27017/movies", {
            useNewUrlParser: true,
            useUnifiedTopology: true
          }, function (error, db) {
            if (error)
              console.log('nie dziala');
            var dbo = db.db("movies");
            // --$or--
            // var languageone = { original_language: /en/ };
            // var languagetwo = { original_language: /de/ };
            // dbo.collection("table").find({ $and: [{ $or: [languageone, languagetwo] }] }).project({ _id: 0, title: 1 }).limit(1).skip(r).next(function (err, result) {
            //   if (err)
            //     throw err;
            //   console.log(JSON.stringify(result.title));
            //   console.log(result.title);
            //   zm2 = result.title;
            //   resolve();
            // });
            // --$and--
            var languageone = { spoken_languages: /en/ };
            var languagetwo = { spoken_languages: /es/ };
            dbo.collection("table").find({ $and: [languageone, languagetwo] }).project({ _id: 0, title: 1 }).limit(1).skip(r).next(function (err, result) {
              if (err)
                throw err;
              console.log(JSON.stringify(result.title));
              console.log(result.title);
              zm2 = result.title;
              resolve();
            });
          });
        }
        )
      };
      await getData();
      this.followUpState('RecomendationState').toIntent('MovieRecomendationIntent');
    },

    async ProductionRecomendationIntent() {
      function getData() {
        return new Promise((resolve, reject) => {
          mongoClient.connect("mongodb://localhost:27017/movies", {
            useNewUrlParser: true,
            useUnifiedTopology: true
          }, function (error, db) {
            if (error)
              console.log('nie dziala');
            var dbo = db.db("movies");
            // --$match--
            var productioncompany = { production_companies: /Warner Bros./ };
            dbo.collection("table").find(productioncompany).project({ _id: 0, title: 1 }).limit(1).skip(r).next(function (err, result) {
              if (err)
                throw err;
              console.log(JSON.stringify(result.title));
              console.log(result.title);
              zm2 = result.title;
              resolve();
            });
          });
        }
        )
      };
      await getData();
      this.followUpState('RecomendationState').toIntent('MovieRecomendationIntent');
    },

    async TwoProductionsRecomendationIntent() {
      function getData() {
        return new Promise((resolve, reject) => {
          mongoClient.connect("mongodb://localhost:27017/movies", {
            useNewUrlParser: true,
            useUnifiedTopology: true
          }, function (error, db) {
            if (error)
              console.log('nie dziala');
            var dbo = db.db("movies");
            // --$or--
            // var productionone = { production_companies: /Warner Bros./ };
            // var productiontwo = { production_companies: /Walt Disney Pictures/ };
            // dbo.collection("table").find({ $and: [{ $or: [productionone, productiontwo] }] }).project({ _id: 0, title: 1 }).limit(1).skip(r).next(function (err, result) {
            //   if (err)
            //     throw err;
            //   console.log(JSON.stringify(result.title));
            //   console.log(result.title);
            //   zm2 = result.title;
            //   resolve();
            // });
            // --$and--
            var productionone = { production_companies: /Warner Bros./ };
            var productiontwo = { production_companies: /DreamWorks/ };
            dbo.collection("table").find({ $and: [productionone, productiontwo] }).project({ _id: 0, title: 1 }).limit(1).skip(r).next(function (err, result) {
              if (err)
                throw err;
              console.log(JSON.stringify(result.title));
              console.log(result.title);
              zm2 = result.title;
              resolve();
            });
          });
        }
        )
      };
      await getData();
      this.followUpState('RecomendationState').toIntent('MovieRecomendationIntent');
    },

    async CountryRecomendationIntent() {
      function getData() {
        return new Promise((resolve, reject) => {
          mongoClient.connect("mongodb://localhost:27017/movies", {
            useNewUrlParser: true,
            useUnifiedTopology: true
          }, function (error, db) {
            if (error)
              console.log('nie dziala');
            var dbo = db.db("movies");
            // --$match--
            var productioncountry = { production_countries: /France/ };
            dbo.collection("table").find(productioncountry).project({ _id: 0, title: 1 }).limit(1).skip(r).next(function (err, result) {
              if (err)
                throw err;
              console.log(JSON.stringify(result.title));
              console.log(result.title);
              zm2 = result.title;
              resolve();
            });
          });
        }
        )
      };
      await getData();
      this.followUpState('RecomendationState').toIntent('MovieRecomendationIntent');
    },

    async TwoCountriesRecomendationIntent() {
      function getData() {
        return new Promise((resolve, reject) => {
          mongoClient.connect("mongodb://localhost:27017/movies", {
            useNewUrlParser: true,
            useUnifiedTopology: true
          }, function (error, db) {
            if (error)
              console.log('nie dziala');
            var dbo = db.db("movies");
            // --$or--
            // var countryone = { production_countries: /United Kingdom/ };
            // var countrytwo = { production_countries: /France/ };
            // dbo.collection("table").find({ $and: [{ $or: [countryone, countrytwo] }] }).project({ _id: 0, title: 1 }).limit(1).skip(r).next(function (err, result) {
            //   if (err)
            //     throw err;
            //   console.log(JSON.stringify(result.title));
            //   console.log(result.title);
            //   zm2 = result.title;
            //   resolve();
            // });
            // --$and--
            var countryone = { production_countries: /Poland/ };
            var countrytwo = { production_countries: /France/ };
            dbo.collection("table").find({ $and: [countryone, countrytwo] }).project({ _id: 0, title: 1 }).limit(1).skip(r).next(function (err, result) {
              if (err)
                throw err;
              console.log(JSON.stringify(result.title));
              console.log(result.title);
              zm2 = result.title;
              resolve();
            });
          });
        }
        )
      };
      await getData();
      this.followUpState('RecomendationState').toIntent('MovieRecomendationIntent');
    },

    async BetweenRecomendationIntent() {
      function getData() {
        return new Promise((resolve, reject) => {
          mongoClient.connect("mongodb://localhost:27017/movies", {
            useNewUrlParser: true,
            useUnifiedTopology: true
          }, function (error, db) {
            if (error)
              console.log('nie dziala');
            var dbo = db.db("movies");
            var betweenone = { release_date: { $gte: "2014-01-01" } };
            var betweentwo = { release_date: { $lte: "2015-12-31" } };
            // var betweenone = { release_date: { $gte: "1998-01-01" } };
            // var betweentwo = { release_date: { $lte: "2000-12-31" } };
            dbo.collection("table").find({ $and: [betweenone, betweentwo] }).project({ _id: 0, title: 1 }).limit(1).skip(r).next(function (err, result) {
              if (err)
                throw err;
              console.log(JSON.stringify(result.title));
              console.log(result.title);
              zm2 = result.title;
              resolve();
            });
          });
        }
        )
      };
      await getData();
      this.followUpState('RecomendationState').toIntent('MovieRecomendationIntent');
    },

    async GenreAndActorRecomendationIntent() {
      function getData() {
        return new Promise((resolve, reject) => {
          mongoClient.connect("mongodb://localhost:27017/movies", {
            useNewUrlParser: true,
            useUnifiedTopology: true
          }, function (error, db) {
            if (error)
              console.log('nie dziala');
            var dbo = db.db("movies");
            // --$and--
            var moviegenre = { genres: /Comedy/ };
            var movieactor = { cast: /Adam Sandler/ };
            dbo.collection("table").find({ $and: [moviegenre, movieactor] }).project({ _id: 0, title: 1 }).limit(1).skip(r).next(function (err, result) {
              if (err)
                throw err;
              console.log(JSON.stringify(result.title));
              console.log(result.title);
              zm2 = result.title;
              resolve();
            });
          });
        }
        )
      };
      await getData();
      this.followUpState('RecomendationState').toIntent('MovieRecomendationIntent');
    },

    async ActorAndLanguageRecomendationIntent() {
      function getData() {
        return new Promise((resolve, reject) => {
          mongoClient.connect("mongodb://localhost:27017/movies", {
            useNewUrlParser: true,
            useUnifiedTopology: true
          }, function (error, db) {
            if (error)
              console.log('nie dziala');
            var dbo = db.db("movies");
            // --$and--
            var movieactor = { cast: /Jackie Chan/ };
            var movielanguage = { original_language: /ru/ };
            dbo.collection("table").find({ $and: [movieactor, movielanguage] }).project({ _id: 0, title: 1 }).limit(1).skip(r).next(function (err, result) {
              if (err)
                throw err;
              console.log(JSON.stringify(result.title));
              console.log(result.title);
              zm2 = result.title;
              resolve();
            });
          });
        }
        )
      };
      await getData();
      this.followUpState('RecomendationState').toIntent('MovieRecomendationIntent');
    },

    async KeywordsOrGenreRecomendationIntent() {
      function getData() {
        return new Promise((resolve, reject) => {
          mongoClient.connect("mongodb://localhost:27017/movies", {
            useNewUrlParser: true,
            useUnifiedTopology: true
          }, function (error, db) {
            if (error)
              console.log('nie dziala');
            var dbo = db.db("movies");
            // --$or--
            var moviekeyword = { keywords: /scary/ };
            var moviegenre = { genres: /Horror/ };
            dbo.collection("table").find({ $and: [{ $or: [moviekeyword, moviegenre] }] }).project({ _id: 0, title: 1 }).limit(1).skip(r).next(function (err, result) {
              if (err)
                throw err;
              console.log(JSON.stringify(result.title));
              console.log(result.title);
              zm2 = result.title;
              resolve();
            });
          });
        }
        )
      };
      await getData();
      this.followUpState('RecomendationState').toIntent('MovieRecomendationIntent');
    },

    async LanguageOrCountryRecomendationIntent() {
      function getData() {
        return new Promise((resolve, reject) => {
          mongoClient.connect("mongodb://localhost:27017/movies", {
            useNewUrlParser: true,
            useUnifiedTopology: true
          }, function (error, db) {
            if (error)
              console.log('nie dziala');
            var dbo = db.db("movies");
            // --$or--
            var movielanguage = { original_language: /fr/ };
            var productioncountry = { production_countries: /Norway/ };
            dbo.collection("table").find({ $and: [{ $or: [movielanguage, productioncountry] }] }).project({ _id: 0, title: 1 }).limit(1).skip(r).next(function (err, result) {
              if (err)
                throw err;
              console.log(JSON.stringify(result.title));
              console.log(result.title);
              zm2 = result.title;
              resolve();
            });
          });
        }
        )
      };
      await getData();
      this.followUpState('RecomendationState').toIntent('MovieRecomendationIntent');
    },

    async ProductionAndYearRecomendationIntent() {
      function getData() {
        return new Promise((resolve, reject) => {
          mongoClient.connect("mongodb://localhost:27017/movies", {
            useNewUrlParser: true,
            useUnifiedTopology: true
          }, function (error, db) {
            if (error)
              console.log('nie dziala');
            var dbo = db.db("movies");
            // --$and--
            var productioncompany = { production_companies: /Universal Pictures/ };
            var betweenone = { release_date: { $gte: "2010-01-01" } };
            var betweentwo = { release_date: { $lte: "2019-12-31" } };
            var beetween = { $and: [betweenone, betweentwo] };
            dbo.collection("table").find({ $and: [productioncompany, beetween] }).project({ _id: 0, title: 1 }).limit(1).skip(r).next(function (err, result) {
              if (err)
                throw err;
              console.log(JSON.stringify(result.title));
              console.log(result.title);
              zm2 = result.title;
              resolve();
            });
          });
        }
        )
      };
      await getData();
      this.followUpState('RecomendationState').toIntent('MovieRecomendationIntent');
    },

    async CountryOrYearRecomendationIntent() {
      function getData() {
        return new Promise((resolve, reject) => {
          mongoClient.connect("mongodb://localhost:27017/movies", {
            useNewUrlParser: true,
            useUnifiedTopology: true
          }, function (error, db) {
            if (error)
              console.log('nie dziala');
            var dbo = db.db("movies");
            // --$or--
            var productioncountry = { production_countries: /China/ };
            var releasedate = { release_date: /2013/ };
            dbo.collection("table").find({ $and: [{ $or: [productioncountry, releasedate] }] }).project({ _id: 0, title: 1 }).limit(1).skip(r).next(function (err, result) {
              if (err)
                throw err;
              console.log(JSON.stringify(result.title));
              console.log(result.title);
              zm2 = result.title;
              resolve();
            });
          });
        }
        )
      };
      await getData();
      this.followUpState('RecomendationState').toIntent('MovieRecomendationIntent');
    },

    MovieRecomendationIntent() {
      console.log("wyniki to:");
      console.log(zm2);
      b = moment().toISOString();
      console.log(a);
      console.log(b);
      console.log(JSON.stringify(r));
      // console.log(a.diff(b,'seconds'));
      this.ask('How about a movie called ' + zm2 + '?');
    }

  },

  ByeIntent() {
    this.$speech.addText([
      'I\'m glad I could help you!',
      'I hope you\'ll enjoy.',
      'Enjoy!',
      'You\'re welcome.',
      'I thought you might like that.',
      'No problem',
      'Great! I\'m glad I could help.',
      'I hope you\'ll like it.',
      'Great choice!',
      'Glad to help!',
      'Don\'t forget to buy popcorn and coke.',
      'Everything for you.',
      'Have a nice watch.',
      'That\'s great!'
    ]);
    this.tell(this.$speech);
  },

  Unhandled() {
    return this.toIntent('LAUNCH');
  }

});

module.exports.app = app;
